package woodstock

import (
	"net/url"
	"testing"
	"time"
)

func TestGetChannel(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "1001"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channel, err := client.GetChannel(channelID)
	if err != nil {
		t.Error(err)
	}
	if channel.Data.ID != channelID {
		t.Errorf("Channel ID doesn't match, got: %s, want: %s", channel.Data.ID, channelID)
	}
	time.Sleep(Delay)
}

func TestGetChannels(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelIDs := []string{"1000", "1001"}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channels, err := client.GetChannels(channelIDs)
	if err != nil {
		t.Error(err)
	}
	if channels.Data[0].ID != channelIDs[0] {
		t.Errorf("Channel doesn't match, got %s, want %s", channels.Data[0].ID, channelIDs[0])
	}
	if channels.Data[1].ID != channelIDs[1] {
		t.Errorf("Channel doesn't match, got %s, want %s", channels.Data[1].ID, channelIDs[1])
	}
	time.Sleep(Delay)
}

func TestCreateChannel(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	//payload := `{'type':'com.example.channel','acl':{'full':{'immutable':false,'you':true,'user_ids':[]},'write':{'any_user':true,'immutable':false,'you':true,'user_ids':[]},'read':{'any_user':true,'immutable':false,'you':true,'user_ids':[]}}}`
	var acl ACL
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channel, err := client.CreateChannel("com.example.channel", acl)
	if err != nil {
		t.Error(err)
	}
	t.Log(channel.Meta)
	time.Sleep(Delay)
}

func TestUpdateChannel(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "1333"
	acl := ACL{
		Read:  Read{Public: true},
		Write: Write{AnyUser: true},
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channel, err := client.UpdateChannel(channelID, acl)
	if err != nil {
		t.Error(err)
	}
	t.Log(channel.Meta)
	time.Sleep(Delay)
}

func TestDeleteChannel(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	var acl ACL
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	newchannel, err := client.CreateChannel("com.example.channel", acl)
	if err != nil {
		t.Error(err)
	}
	channel, err := client.DeleteChannel(newchannel.Data.ID)
	if err != nil {
		t.Error(err)
	}
	t.Log(channel.Meta)
	time.Sleep(Delay)
}

func TestGetSubscribersOfChannel(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "600"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("include_presence", "1")
	subs, err := client.GetSubscribersOfChannel(channelID, params)
	if err != nil {
		t.Error(err)
	}
	t.Log(subs.Meta)
	time.Sleep(Delay)
}

func TestSubscribeChannel(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "178"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channel, err := client.SubscribeChannel(channelID)
	if err != nil {
		t.Error(err)
	}
	if channel.Data.YouSubscribed != true {
		t.Errorf("YouSubscribed is not true")
	}
	time.Sleep(Delay)
}

func TestUnSubscribeChannel(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "178"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channel, err := client.UnSubscribeChannel(channelID)
	if err != nil {
		t.Error(err)
	}
	if channel.Data.YouSubscribed != false {
		t.Errorf("YouSubscribed is not false")
	}
	time.Sleep(Delay)
}

func TestMuteChannel(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "178"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channel, err := client.MuteChannel(channelID)
	if err != nil {
		t.Error(err)
	}
	if channel.Data.YouMuted != true {
		t.Errorf("YouMuted is not true")
	}
	time.Sleep(Delay)
}

func TestUnMuteChannel(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "178"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channel, err := client.UnMuteChannel(channelID)
	if err != nil {
		t.Error(err)
	}
	if channel.Data.YouMuted != false {
		t.Errorf("YouMuted is not false")
	}
	time.Sleep(Delay)
}

func TestGetMessage(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "1001"
	messageID := "298287"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("include_message_raw", "1")
	message, err := client.GetMessage(channelID, messageID, params)
	if err != nil {
		t.Error(err)
	}
	if message.Data.ID != messageID {
		t.Errorf("Got the wrong message, got: %s, want: %s", message.Data.ID, messageID)
	}
	time.Sleep(Delay)
}

func TestGetMessagesInThread(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "178"
	messageID := "106786"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("include_message_raw", "1")
	messages, err := client.GetMessagesInThread(channelID, messageID, params)
	if err != nil {
		t.Error(err)
	}
	t.Log(messages.Meta)
	time.Sleep(Delay)
}

func TestGetMessages(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	messageIDs := []string{"106786", "106785"}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("include_message_raw", "1")
	messages, err := client.GetMessages(messageIDs, params)
	if err != nil {
		t.Error(err)
	}
	t.Log(messages.Meta)
	time.Sleep(Delay)
}

func TestGetMessagesOfChannel(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "178"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("include_message_raw", "1")
	messages, err := client.GetMessagesOfChannel(channelID, params)
	if err != nil {
		t.Error(err)
	}
	t.Log(messages.Meta)
	time.Sleep(Delay)
}

func TestCreateMessage(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "1001"
	text := "golang rocks!"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)

	language := map[string]string{"language": "en"}
	fileembed := map[string]string{
		"file_id":    "24329",
		"file_token": "8sRtHLXlqg-zHWLE-5XDW1mttHBK9-oq",
		"format":     "oembed",
	}
	oembed := map[string]map[string]string{
		"+io.pnut.core.file": fileembed,
	}
	raw := map[string]interface{}{
		"io.pnut.core.language": []map[string]string{language},
		"io.pnut.core.oembed":   []map[string]map[string]string{oembed},
	}

	newmessage := NewMessage{Text: text, Raw: raw}
	message, err := client.CreateMessage(channelID, newmessage, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if message.Data.Content.Text != text {
		t.Errorf("Message does not match, got: %s, want: %s", message.Data.Content.Text, text)
	}
	time.Sleep(Delay)
}

func TestDeleteMessage(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "1001"
	text := "golang rocks!"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	var raw map[string]string
	newmessage := NewMessage{Text: text, Raw: raw}
	newmessager, err := client.CreateMessage(channelID, newmessage, url.Values{})
	if err != nil {
		t.Error(err)
	}
	message, err := client.DeleteMessage(channelID, newmessager.Data.ID)
	if err != nil {
		t.Error(err)
	}
	if message.Data.IsDeleted != true {
		t.Errorf("Message does not show as deleted.")
	}
	time.Sleep(Delay)
}

func TestGetStickyMessages(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "212"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	messages, err := client.GetStickyMessages(channelID)
	if err != nil {
		t.Error(err)
	}
	t.Log(messages.Meta)
	time.Sleep(Delay)
}

func TestSticky(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "1001"
	messageID := "106951"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	message, err := client.Sticky(channelID, messageID)
	if err != nil {
		t.Error(err)
	}
	t.Log(message.Meta)
	time.Sleep(Delay)
}

func TestUnSticky(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	channelID := "1001"
	messageID := "106951"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	message, err := client.UnSticky(channelID, messageID)
	if err != nil {
		t.Error(err)
	}
	t.Log(message.Meta)
	time.Sleep(Delay)
}
