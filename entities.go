package woodstock

// Links object definition
type Links struct {
	AmendedLen int    `json:"amended_len"`
	Len        int    `json:"len"`
	Pos        int    `json:"pos"`
	Text       string `json:"text"`
	Title      string `json:"title"`
	URL        string `json:"url"`
}

// Mentions object definition
type Mentions struct {
	ID        string `json:"id"`
	IsCopy    bool   `json:"is_copy"`
	IsLeading bool   `json:"is_leading"`
	Len       int    `json:"len"`
	Pos       int    `json:"pos"`
	Text      string `json:"text"`
}

// Tags object definition
type Tags struct {
	Len  int    `json:"len"`
	Pos  int    `json:"pos"`
	Text string `json:"text"`
}

// Entities object definition
type Entities struct {
	Links    []Links    `json:"links"`
	Mentions []Mentions `json:"mentions"`
	Tags     []Tags     `json:"tags"`
}
