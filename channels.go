package woodstock

import (
	"encoding/json"
	"net/url"
	"strings"
)

// ChannelResult is a response containing a channel object
type ChannelResult struct {
	*CommonResponse
	Data Channel `json:"data"`
}

// ChannelsResult is a response containing multiple channel objects
type ChannelsResult struct {
	*CommonResponse
	Data []Channel `json:"data"`
}

type createChannel struct {
	Type string `json:"type"`
	ACL  ACL    `json:"acl"`
}

// NewMessage object definition
type NewMessage struct {
	Text               string      `json:"text"`
	ParseLinks         string      `json:"entities.parse_links,omitempty"`
	ParseMarkdownLinks string      `json:"entities.parse_markdown_links,omitempty"`
	Nsfw               string      `json:"is_nsfw,omitempty"`
	Raw                interface{} `json:"raw"`
	ReplyTo            string      `json:"reply_to,omitempty"`
}

// GetChannel retrieves the channel object from the channel ID
// https://pnut.io/docs/resources/channels/lookup#get-channels-id
func (c *Client) GetChannel(id string) (result ChannelResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + id, data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetChannels retrieves a list of channels from the provided channel IDs
// https://pnut.io/docs/resources/channels/lookup#get-channels
func (c *Client) GetChannels(ids []string) (result UsersResult, err error) {
	qparams := url.Values{}
	qparams.Set("ids", strings.Join(ids, ","))

	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// CreateChannel creates a new channel
// https://pnut.io/docs/resources/channels/lifecycle#post-channels
func (c *Client) CreateChannel(typeStr string, acl ACL) (result ChannelResult, err error) {
	json, err := json.Marshal(&createChannel{Type: typeStr, ACL: acl})
	if err != nil {
		return
	}
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI, data: &result, method: "POST", responseCh: responseCh, json: string(json)}
	return result, (<-responseCh).err
}

// UpdateChannel updates an existing channel
// https://pnut.io/docs/resources/channels/lifecycle#put-channels-id
func (c *Client) UpdateChannel(id string, acl ACL) (result ChannelResult, err error) {
	json, err := json.Marshal(&createChannel{ACL: acl})
	if err != nil {
		return
	}
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + id, data: &result, method: "PUT", responseCh: responseCh, json: string(json)}
	return result, (<-responseCh).err
}

// DeleteChannel deletes an existing channel
// https://pnut.io/docs/resources/channels/lifecycle#delete-channels-id
func (c *Client) DeleteChannel(id string) (result ChannelResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + id, data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetSubscribersOfChannel retrieves a list of subscribers from a channel ID
// https://pnut.io/docs/resources/channels/subscribing#get-channels-id-subscribers
func (c *Client) GetSubscribersOfChannel(id string, qparams url.Values) (result UsersResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + id + "/subscribers?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// SubscribeChannel subscribes the authenticated user to a channel
// https://pnut.io/docs/resources/channels/subscribing#put-channels-id-subscribe
func (c *Client) SubscribeChannel(id string) (result ChannelResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + id + "/subscribe", data: &result, method: "PUT", responseCh: responseCh}
	return result, (<-responseCh).err
}

// UnSubscribeChannel unsubscribes the authenticated user from a channel
// https://pnut.io/docs/resources/channels/subscribing#delete-channels-id-subscribe
func (c *Client) UnSubscribeChannel(id string) (result ChannelResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + id + "/subscribe", data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}

// MuteChannel mutes notifications for a channel
// https://pnut.io/docs/resources/channels/muting#put-channels-id-mute
func (c *Client) MuteChannel(id string) (result ChannelResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + id + "/mute", data: &result, method: "PUT", responseCh: responseCh}
	return result, (<-responseCh).err
}

// UnMuteChannel unmutes notifications for a channel
// https://pnut.io/docs/resources/channels/muting#delete-channels-id-mute
func (c *Client) UnMuteChannel(id string) (result ChannelResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + id + "/mute", data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetMessage retrieves a specific message from a channel
// https://pnut.io/docs/resources/messages/lookup#get-channels-id-messages-id
func (c *Client) GetMessage(channelID string, messageID string, qparams url.Values) (result PostResult, err error) {
	responseCh := make(chan response)
	url := ChannelAPI + "/" + channelID + "/messages/" + messageID + "?" + qparams.Encode()
	c.queryQueue <- query{url: url, data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetMessagesInThread retrieves messages in a thread from a channel
// https://pnut.io/docs/resources/messages/lookup#get-channels-id-messages-id-thread
func (c *Client) GetMessagesInThread(channelID string, messageID string, qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	url := ChannelAPI + "/" + channelID + "/messages/" + messageID + "/thread?" + qparams.Encode()
	c.queryQueue <- query{url: url, data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetMessages retrieves multiple messages
// https://pnut.io/docs/resources/messages/lookup#get-messages
func (c *Client) GetMessages(ids []string, qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	qparams.Set("ids", strings.Join(ids, ","))
	url := ChannelAPI + "/messages?" + qparams.Encode()
	c.queryQueue <- query{url: url, data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetMessagesOfChannel retrieves messages bashed on channel ID
// https://pnut.io/docs/resources/messages/lookup#get-channels-id-messages
func (c *Client) GetMessagesOfChannel(id string, qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	url := ChannelAPI + "/" + id + "/messages?" + qparams.Encode()
	c.queryQueue <- query{url: url, data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// CreateMessage creates a new message
// https://pnut.io/docs/resources/messages/lifecycle#post-channels-id-messages
func (c *Client) CreateMessage(id string, msg NewMessage, qparams url.Values) (result PostResult, err error) {
	json, err := json.Marshal(msg)
	if err != nil {
		return
	}

	responseCh := make(chan response)
	url := ChannelAPI + "/" + id + "/messages?" + qparams.Encode()
	c.queryQueue <- query{url: url, data: &result, method: "POST", responseCh: responseCh, json: string(json)}
	return result, (<-responseCh).err
}

// DeleteMessage deletes a specific message
// https://pnut.io/docs/resources/messages/lifecycle#delete-channels-id-messages-id
func (c *Client) DeleteMessage(channelID string, messageID string) (result PostResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + channelID + "/messages/" + messageID, data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetStickyMessages retrives sticky messages
// https://pnut.io/docs/resources/messages/sticky#get-channels-id-sticky_messages
func (c *Client) GetStickyMessages(id string) (result PostsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + id + "/sticky_messages", data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// Sticky sets a message as sticky
// https://pnut.io/docs/resources/messages/sticky#put-channels-id-messages-id-sticky
func (c *Client) Sticky(channelID string, messageID string) (result PostResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + channelID + "/messages/" + messageID + "/sticky", data: &result, method: "PUT", responseCh: responseCh}
	return result, (<-responseCh).err
}

// UnSticky unsets a message as sticky
// https://pnut.io/docs/resources/messages/sticky#delete-channels-id-messages-id-sticky
func (c *Client) UnSticky(channelID string, messageID string) (result PostResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ChannelAPI + "/" + channelID + "/messages/" + messageID + "/sticky", data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}
