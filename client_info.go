package woodstock

// ClientInfoResult object, https://pnut.io/docs/resources/clients
type ClientInfoResult struct {
	*CommonResponse
	Data ClientInfo `json:"data"`
}

// GetClient retrieves client object based on the id
// https://pnut.io/docs/resources/clients#get-clients-id
func (c *Client) GetClient(id string) (result ClientInfoResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: ClientAPI + "/" + id, data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}
